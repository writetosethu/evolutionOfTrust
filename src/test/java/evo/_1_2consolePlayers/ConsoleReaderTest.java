package evo._1_2consolePlayers;

import evo._1_2consolePlayers.ConsoleReader;
import org.junit.After;
import org.junit.Test;

import java.io.ByteArrayInputStream;

import static org.assertj.core.api.Assertions.assertThat;

public class ConsoleReaderTest {

    @Test
    public void shouldReturnStringFromConsole() {
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream("Cheat".getBytes());
        System.setIn(byteArrayInputStream);
        ConsoleReader consoleReader = new ConsoleReader();
        assertThat(consoleReader.readInput()).isEqualTo("Cheat");
    }

    @After
    public void resetSystemInputStream() {
        System.setIn(System.in);
    }
}